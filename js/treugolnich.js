////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
////////// TREUGOLNICH.JS //////////
var
// размер треугольника
min_size = 29,
max_size = 31,
// количество елементов (только если разкоментировать рандом)
col_of_elems = 500,
// на какое количество пикселей движеться (размер ромба)
toPixJeyson = 10,
// прозрачность треугольников
min_visibility = 30,
max_visibility = 60,
// задать rgb треугольникам
colorR = 255,
colorG = 255,
colorB = 0,
// размеры канваса для екрана(сколько % мы упускаем)
screenProc = 1.26;
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
// обьявляем канвас
var canvas;
var ctx;
// масив треугольников
var triangle = [];
window.onload = function() {
	// обьявляем canvas
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");
	// задаем размеры канвасу как окну
	var innerWidthPr = window.innerWidth/100,
	innerHeightPr = window.innerHeight/100;
	canvas.width = window.innerWidth-(innerWidthPr*screenProc);
	canvas.height = window.innerHeight-(innerHeightPr*screenProc);
	var w_y = canvas.width;
	var w_x = canvas.height;
	////////////////////////////////////
	////////////////////////////////////
	// разкоментируй сдесь что бы работал рандом спавн (сейчас работает)
	////////////////////////////////////
	////////////////////////////////////
	// создаем обьекты
	for (var i = 0; i < col_of_elems; i++) {
		// вызываем функцию создания елемента
		trigons(rand(50,w_x-50),rand(50,w_y-50));
	}
	////////////////////////////////////
	////////////////////////////////////
	//////Сюда вырисовывать обьект (это без рандома(сейчас не работает))//////
	////////////////////////////////////
	////////////////////////////////////
	// trigons(x,y);
	// START
	// trigons(100,100);
	// trigons(160,90);
	// trigons(210,110);
	// trigons(270,100);
	// END
	fuTeMaTik();
	////////////////////////////////////
	////////////////////////////////////
	//////Сюда вырисовывать обьект//////
	////////////////////////////////////
	////////////////////////////////////

	// итог это функция jeyson
	function jeysonDraw() {
		// очистка поля при каждом повторе
		ctx.clearRect(0,0,canvas.width,canvas.height);
		// аним
		function animate() {
			if (jeyson <= toPixJeyson) {
				triangle[i].x++;
				triangle[i].y--;
			} else if (jeyson <= toPixJeyson*2) {
				triangle[i].x++;
				triangle[i].y++;
			} else if (jeyson <= toPixJeyson*3) {
				triangle[i].x--;
				triangle[i].y++;
			} else if (jeyson <= toPixJeyson*4) {
				triangle[i].x--;
				triangle[i].y--;
			} else {
			triangle[i].jeyson = 0;
			jeyson = 0;
			}
		}
		// рисуем всё что должно быть на поле
		for (var i = 0; i < triangle.length; i++) {
			// главная последовательность движений при перерисовке
			var jeyson = triangle[i].jeyson;
			setTimeout(animate(), 100);
			// рисование на холсте
			draw(triangle[i].x,triangle[i].y,triangle[i].size,triangle[i].visibility);
			// увеличиваем переменую которая управляет воображаемым цыклом
			triangle[i].jeyson++;
		}
		// говорим окну что бы оно повторяло ету функцию
		window.requestAnimationFrame(jeysonDraw);
	}

	// This is the end
	// Hold your breath and count to ten
	// Feel the earth move and then
	// Hear my heart burst again
	//
	// For this is the end
	// I’ve drowned and dreamt this moment
	// So overdue I owe them
	// Swept away, I’m stolen
	jeysonDraw();
	//
	// Let the sky fall
	// When it crumbles
	// We will stand tall
	// Face it all together
	//
	// Let the sky fall
	// When it crumbles
	// We will stand tall
	// Face it all together
	// At skyfall
	// At skyfall

// end of okno
}
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
function inRad(num) {
	// я ведь говорил, что функция принимает угол в радианах?
	return num * Math.PI / 180;
}
function rand(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //Включно з мінімальним та виключаючи максимальне значення
}
// создание елемента
function trigons(x,y) {
	triangle.push({
		x : x,
		y : y,
		size : rand(min_size,max_size),
		visibility : rand(min_visibility,max_visibility)
	});
}
// добавление джейсона в обьекты
function fuTeMaTik() {
	for (var i = 0; i < triangle.length; i++) {
		triangle[i].jeyson = rand(1,toPixJeyson*4);
		triangle[i].jeysonStar = rand(1,toPixJeyson*4);
	}
}
// рисование обьекта
function draw(x,y,size,visibility) {
  // треугольник
  ctx.fillStyle = "rgba("+colorR+","+colorG+","+colorB+",."+visibility+")";
  ctx.lineWidth = 5;
  // рисование треугольника
  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(x+size, y);
  ctx.lineTo(x+(size/2), y-size);
  ctx.closePath();
  // закрашивание
  ctx.fill();
	// конец
}
// this project made for 2 fucking mounth by Andrey Bondarenko
